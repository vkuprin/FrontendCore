const path = require(`path`);
const webpack = require(`webpack`);
const { argv } = require(`yargs`);

const MiniCssExtractPlugin = require(`mini-css-extract-plugin`);
const TerserPlugin = require(`terser-webpack-plugin`);

const isDevelopment = argv.mode === `development`;
const isProduction = !isDevelopment;

const config = {
    output: {
        path: path.resolve(__dirname, `../src/static/dist/`),
        publicPath: `./static/dist/`,
        filename: `[name].js`
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: `babel-loader`,
                options: {
                    sourceMap: true
                }
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: `style-loader`,
                        options: {
                            sourceMap: true
                        }
                    }, {
                        loader: `css-loader`,
                        options: {
                            sourceMap: true,
                            modules: true
                        }
                    }
                ]
            },
            {
                test: /\.(scss|less)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            sourceMap: true,
                            modules: true
                        }
                    },
                    {
                        loader: `css-loader`,
                        options: {
                            sourceMap: true,
                            modules: true
                        }
                    },
                    {
                        loader: `sass-loader`,
                        options: {
                            sourceMap: true,
                            modules: true
                        }
                    },
                    {
                        loader: `less-loader`,
                        options: {
                            sourceMap: true,
                            modules: true
                        }
                    }
                ]
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/,
                use: [
                    `file-loader`
                ]
            }
        ]
    },
    devtool: isProduction ? `none` : `inline-source-map`,
    resolve: { extensions: [`*`, `.js`, `.jsx`] },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new MiniCssExtractPlugin({
            filename: `[name].css`,
            chunkFilename: `[name].css`
        })
    ],
    optimization: isProduction ? {
        minimize: true,
        minimizer: [new TerserPlugin()]
    } : {}
};

module.exports = config;
