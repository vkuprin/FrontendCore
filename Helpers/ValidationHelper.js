const ValidationHelper = {
    required: str => str.length > 0,
    isNumber: str => /^[-+]*[\d]*$/.test(str),
    isNatural: str => parseInt(str, 10) >= 0,
    isNotEmptyArray: arr => arr.length > 0
};

export default ValidationHelper;
