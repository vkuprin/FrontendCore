function encodeQueryString(params) {
    const keys = Object.keys(params);
    return keys.length
        ? `?${keys
            .map(key => `${encodeURIComponent(key)
            }=${encodeURIComponent(params[key])}`)
            .join(`&`)}`
        : ``;
}

const FetchHelper = {
    get: (url_to, data = {}) => {
        const url = url_to + encodeQueryString(data);
        return fetch(url, {
            method: `GET`,
            headers: {},
            credentials: `include`
        });
    },

    post: (url_to, data = {}) => {
        const formData = new FormData();
        for (let i = 0; i < Object.keys(data).length; i++) {
            const key = Object.keys(data)[i];
            formData.append(key, data[key]);
        }
        return fetch(url_to, {
            method: `POST`,
            headers: {},
            body: formData,
            credentials: `include`
        });
    },

    get_json: (url_to, data = {}) => FetchHelper.get(url_to, data)
        .then(resp => resp.json()
            .then(data => data)),

    post_json: (url_to, data = {}) => FetchHelper.post(url_to, data)
        .then(resp => resp.json()
            .then(data => data))
};

export default FetchHelper;
