import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.less';

const Loader = (props) => {
    const {isLoading, className, style} = props;
    return (<div
        className={`${isLoading ? styles.loader : styles.unloader} ${className}`}
        style={style}
    />);
}

Loader.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    className: PropTypes.string,
    style: PropTypes.object
};

Loader.defaultProps = {
    className: ``,
    style: {}
};

export default Loader;
