import { action } from 'mobx';

class Actions {
    @action addLoading = () => {
        this.countOfLoads += 1;
    };
    @action removeLoading = () => {
        if (this.countOfLoads > 0) {
            this.countOfLoads -= 1;
        }
    };
    @action stopLoading = () => {
        this.countOfLoads = 0;
    };
}

export default Actions;
