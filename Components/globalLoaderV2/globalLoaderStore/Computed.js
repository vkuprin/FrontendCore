import { computed } from 'mobx';
import Actions from './Actions';

class Computed extends Actions {
    @computed get isLoading() {
        return this.countOfLoads > 0;
    }
}

export default Computed;
