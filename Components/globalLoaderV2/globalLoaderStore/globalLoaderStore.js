import { observable } from 'mobx';
import Computed from './Computed';

class GlobalLoaderStore extends Computed {
    @observable countOfLoads = 0;
}

export default new GlobalLoaderStore();
