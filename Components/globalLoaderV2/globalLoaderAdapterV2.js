import globalLoaderStore from './globalLoaderStore/globalLoaderStore';


const globalLoaderAdapterV2 = {
    addLoading: () => globalLoaderStore.addLoading(),
    removeLoading: () => globalLoaderStore.removeLoading(),
    stopLoading: () => globalLoaderStore.stopLoading()
};

export default globalLoaderAdapterV2;
