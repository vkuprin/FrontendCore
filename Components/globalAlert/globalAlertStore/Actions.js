import { action } from 'mobx';

class Actions {
    @action setAlerts = (alerts) => {
        this.alerts = alerts;
    };
    @action addAlert = (alert) => {
        this.alerts.push(alert);
    };
    @action shiftAlert = () => {
        this.alerts.shift();
    };
}

export default Actions;
