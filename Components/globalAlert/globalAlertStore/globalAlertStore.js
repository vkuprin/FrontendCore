import { observable } from 'mobx';
import Computed from './Computed';

class GlobalAlertStore extends Computed {
    @observable alerts = [];
}

export default new GlobalAlertStore();
