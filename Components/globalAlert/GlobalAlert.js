import React from 'react';
import PropTypes from 'prop-types';
import DefaultWrapper from './wrappers/DefaultWrapper/DefaultWrapper';
import AlertMessage from './components/AlertMessage';
import globalAlertStore from './globalAlertStore/globalAlertStore';


const GlobalAlert = (props) => {
    let {Wrapper} = props;
    Wrapper = Wrapper || DefaultWrapper;
    const {alerts} = globalAlertStore;

    const alertsRendered = alerts.map((item) => (
        <AlertMessage
            key={item.uid}
            uid={item.uid}
            variant={item.variant}
            title={item.title}
            message={item.message}
            timeout={item.timeout}
        />));

    return (
        <Wrapper>
            {alertsRendered}
        </Wrapper>
    );
}

GlobalAlert.propTypes = {
    Wrapper: PropTypes.oneOf(PropTypes.element, PropTypes.func)
};

GlobalAlert.defaultProps = {
    Wrapper: DefaultWrapper
};

export default GlobalAlert;
