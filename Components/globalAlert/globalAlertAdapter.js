import _ from 'underscore';
import globalAlertStore from './globalAlertStore/globalAlertStore';

const globalAlertVariants = {
    primary: `primary`,
    secondary: `secondary`,
    success: `success`,
    danger: `danger`,
    warning: `warning`,
    info: `info`,
    light: `light`,
    dark: `dark`
};

const timeout = 2000;
const animationTime = 1000;

const addAlert = ({
    variant, title, message
}) => {
    const uid = _.uniqueId();
    globalAlertStore.addAlert({
        uid, variant, title, message, timeout
    });
    setTimeout(() => globalAlertStore.shiftAlert(), timeout + animationTime);
};

const addPrimary = ({ title, message }) => addAlert({ variant: globalAlertVariants.primary, title, message });
const addSecondary = ({ title, message }) => addAlert({ variant: globalAlertVariants.secondary, title, message });
const addSuccess = ({ title, message }) => addAlert({ variant: globalAlertVariants.success, title, message });
const addDanger = ({ title, message }) => addAlert({ variant: globalAlertVariants.danger, title, message });
const addWarning = ({ title, message }) => addAlert({ variant: globalAlertVariants.warning, title, message });
const addInfo = ({ title, message }) => addAlert({ variant: globalAlertVariants.info, title, message });
const addLight = ({ title, message }) => addAlert({ variant: globalAlertVariants.light, title, message });
const addDark = ({ title, message }) => addAlert({ variant: globalAlertVariants.dark, title, message });

const globalAlertAdapter = {
    variants: globalAlertVariants,
    addAlert,
    addPrimary,
    addSecondary,
    addSuccess,
    addDanger,
    addWarning,
    addInfo,
    addLight,
    addDark
};

export { globalAlertAdapter, globalAlertVariants };
