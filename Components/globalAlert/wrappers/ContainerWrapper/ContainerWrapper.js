import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import styles from './styles.less';

const xl = { span: 3, offset: 9 };
const lg = { span: 4, offset: 8 };
const md = { span: 5, offset: 7 };
const sm = { span: 8, offset: 2 };
const xs = { span: 12, offset: 0 };

const ContainerWrapper = (props) => (
    <div className={styles.global_alert}>
        <Container>
            <Row>
                <Col xl={xl} lg={lg} md={md} sm={sm} xs={xs} className={styles.col_alert} {...props} />
            </Row>
        </Container>
    </div>
);

export default ContainerWrapper;
