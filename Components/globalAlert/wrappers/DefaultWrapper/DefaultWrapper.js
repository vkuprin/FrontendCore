import React from 'react';
import styles from './styles.less';

const DefaultWrapper = (props) => <div className={styles.global_alert} {...props} />;

export default DefaultWrapper;
