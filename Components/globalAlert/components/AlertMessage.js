import React from 'react';
import PropTypes from 'prop-types';
import { Alert, Fade } from 'react-bootstrap';


class AlertMessage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        };
    }

    componentDidMount() {
        this.setState({ show: true });
        const { timeout } = this.props;
        setTimeout(() => this.setState({ show: false }), timeout);
    }

    render() {
        const { show } = this.state;
        const {
            variant, title, message
        } = this.props;

        return (
            <Fade show={show}>
                <Alert show={show} variant={variant}>
                    {!!title && <Alert.Heading>
                        {title}
                    </Alert.Heading>}
                    {message}
                </Alert>
            </Fade>);
    }
}

AlertMessage.propTypes = {
    uid: PropTypes.string,
    variant: PropTypes.string.isRequired,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    message: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
    timeout: PropTypes.number.isRequired
};


export default AlertMessage;
