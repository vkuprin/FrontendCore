import DefaultWrapper from './wrappers/DefaultWrapper/DefaultWrapper';
import ContainerWrapper from './wrappers/ContainerWrapper/ContainerWrapper';

const globalAlertWrappers = {
    DefaultWrapper,
    ContainerWrapper
};

export default globalAlertWrappers;
