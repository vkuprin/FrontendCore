import React from 'react';
import { observer } from 'mobx-react';
import Loader from '../loader/Loader';
import globalLoaderStore from './globalLoaderStore/globalLoaderStore';


@observer
class GlobalLoader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            style: {
                zIndex: 100000,
                position: `fixed`,
                top: 0,
                left: 0
            }
        };
    }

    render() {
        const { isLoading } = globalLoaderStore;
        const { style } = this.state;

        return (
            <Loader
                isLoading={isLoading}
                style={style}
            />
        );
    }
}


export default GlobalLoader;
