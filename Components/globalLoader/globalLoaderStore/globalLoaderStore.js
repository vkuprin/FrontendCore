import { observable } from 'mobx';
import Computed from './Computed';

class GlobalLoaderStore extends Computed {
    @observable isLoading = false;
}

export default new GlobalLoaderStore();
