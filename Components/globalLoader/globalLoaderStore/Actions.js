import { action } from 'mobx';

class Actions {
    @action setIsLoading = (isLoading) => {
        this.isLoading = isLoading;
    };
}

export default Actions;
