import globalLoaderStore from './globalLoaderStore/globalLoaderStore';


const globalLoaderAdapter = {
  startLoading: () => globalLoaderStore.setIsLoading(true),
  stopLoading: () => globalLoaderStore.setIsLoading(false),
};

export default globalLoaderAdapter;
