import React from 'react';
import PropTypes from 'prop-types';

const BlankLink = (props) => (
    <a
        className={props.className}
        href={props.href}
        target="_blank"
        rel="noopener noreferrer"
    >
        {props.children}
    </a>
);

BlankLink.propTypes = {
    className: PropTypes.string,
    href: PropTypes.string
};

BlankLink.defaultProps = {
    className: ``,
    href: ``
};


export default BlankLink;
