import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.less';

const AspectRatioBox = (props) => {
    const aspect = (props.y / props.x) * 100;

    return (
        <div className={styles.aspectRatioBox} style={{ paddingTop: `${aspect}%` }}>
            <div className={styles.aspectRatioBoxInside}>
                {props.children}
            </div>
        </div>
    );
};

AspectRatioBox.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number
};
AspectRatioBox.defaultProps = {
    x: 1,
    y: 1
};

export default AspectRatioBox;
