import React from 'react';
import PropTypes from 'prop-types';
import {CSSTransition} from 'react-transition-group';
import style from './styles.less';


const fadeClassNames = {
    appear: style.appear,
    appearActive: style.appearActive,
    appearDone: style.appearDone,
    enter: style.enter,
    enterActive: style.enterActive,
    enterDone: style.enterDone,
    exit: style.exit,
    exitActive: style.exitActive,
    exitDone: style.exitDone
};

const Fade = (props) => {
    return (
        <CSSTransition in={props.show} timeout={300} classNames={fadeClassNames}>
            {props.children}
        </CSSTransition>);
}

Fade.propTypes = {
    show: PropTypes.bool.isRequired
};


export default Fade;
